package org.green.kav.deploy.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

@SpringBootTest
public class JarFileProcessorTest {

//    @Test
    public void testRemoveDir() throws IOException {
        String fileLocation = "";
        File fileDirectories = new File(fileLocation);

        try (Stream<Path> stream = Files.find(Paths.get(fileDirectories.getAbsolutePath()),
                                              20,
                                              (path, attr) -> (path.getFileName().toString().contains(".iml") ||
                                                               path.getFileName().toString().contains(".idea")))) {
            stream.forEach(path -> {
                try {
                    System.err.println(path);
                    Files.delete(path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

//    @Test
    public void textJarProcess() throws IOException {
        String fileLocation =
                "D:\\Users\\sm13\\Documents\\development\\ipf\\be\\branch_develop\\ipf-analysis\\eba\\dataaccess\\serialization\\gson\\target\\classes\\META-INF\\MANIFEST.MF";
        List<String> list = Files.readAllLines(new File(fileLocation).toPath(), Charset.forName("utf-8"));

        List<String> reserved = new ArrayList<>();
        reserved.add("Manifest-Version:");
        reserved.add("Bnd-LastModified:");
        reserved.add("Build-Jdk:");
        reserved.add("Built-By:");
        reserved.add("Bundle-Blueprint:");
        reserved.add("Bundle-Description:");
        reserved.add("Bundle-DocURL:");
        reserved.add("Bundle-ManifestVersion:");
        reserved.add("Bundle-Name:");
        reserved.add("Bundle-SymbolicName:");
        reserved.add("Bundle-Vendor:");
        reserved.add("Bundle-Version:");
        reserved.add("Created-By:");
        reserved.add("Export-Package:");
        reserved.add("Implementation-Version:");
        reserved.add("Import-Package:");
        reserved.add("Include-Resource:");
        reserved.add("Private-Package:");
        reserved.add("Require-Capability:");
        reserved.add("Tool:");

        for (int i = 0; i < list.size(); i++) {
            String data = list.get(i);
            if (!contains(data, reserved)) {
                appendToPreviousLine(list, i, data);
            }
        }

        Map<String, String> keys = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            String bundleConfig = list.get(i).trim();
            if (!bundleConfig.isEmpty()) {
                String configKey = bundleConfig.substring(0, bundleConfig.indexOf(":"));
                String configValue = bundleConfig.substring(bundleConfig.indexOf(":") + 1).trim();
                keys.put(configKey, configValue);
            }
        }
    }

    private boolean contains(String data, List<String> reservedKeys) {
        for (String reservedKey : reservedKeys) {
            if (data.contains(reservedKey)) {
                return true;
            }
        }
        return false;
    }

    private void appendToPreviousLine(List<String> dataLs, int currentIndex, String currentData) {
        int idx = currentIndex - 1;
        while (idx != -1) {
            String previousLineData = dataLs.get(idx);
            System.err.println(previousLineData);
            if (previousLineData.isEmpty()) {
                idx--;
                continue;
            }
            String newData = previousLineData + currentData.trim();
            dataLs.set(currentIndex, "");
            dataLs.set(idx, newData);
            break;
        }
    }
}
