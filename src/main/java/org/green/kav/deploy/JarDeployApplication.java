package org.green.kav.deploy;

import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"org.green.kav.deploy", "org.green.kav.move.remote.io", "org.green.kav.fauna.client"})
public class JarDeployApplication {

	public static void main(String[] args) {
		Application.launch(JarDeployUIApplication.class, args);
	}

}
