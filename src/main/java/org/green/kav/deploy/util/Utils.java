package org.green.kav.deploy.util;

import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import org.green.kav.deploy.controller.event.JarSelectionEventHandler;
import org.green.kav.deploy.service.JarFileProcessor;
import org.green.kav.deploy.service.model.JarFileModel;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Utils {

    public static void createJarCheckBoxes(JarFileProcessor jarFileProcessor,
                                     Map<UUID, JarFileModel> jarFileModelMap,
                                     String absolutePath,
                                     VBox jarDirectoriesVBox,
                                     List<CheckBox> checkBoxList) {
        for (JarFileModel jarFileModel : jarFileProcessor.findJar(new File(absolutePath))) {
            jarFileModelMap.put(jarFileModel.getId(), jarFileModel);
            CheckBox checkedJarFile = new CheckBox(getPath(jarFileModel.getNewDirectoryPaths()));
            checkedJarFile.setId(jarFileModel.getId().toString());
            checkedJarFile.setSelected(true);
            checkedJarFile.setTooltip(new Tooltip(jarFileModel.getDeploymentFile().getAbsolutePath()));
            checkedJarFile.setOnAction(new JarSelectionEventHandler(jarFileModel));
            checkBoxList.add(checkedJarFile);
            jarDirectoriesVBox.getChildren().add(checkedJarFile);

            checkedJarFile.setOnMouseClicked(mouseEvent -> {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) { // Only if double clicked
                        JarFileModel jarFileModel1 = jarFileModelMap.get(UUID.fromString(checkedJarFile.getId()));
                        try {
                            // Just one line and you are done !
                            // We have given a command to start cmd
                            // /K : Carries out command specified by string
                            Runtime.getRuntime().exec("explorer.exe " + jarFileModel1.getDeploymentFile().getParentFile());

                        } catch (Exception e) {
                            System.out.println("HEY Buddy ! U r Doing Something Wrong ");
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    public static String getPath(String[] paths) {
        String newPath = "";
        for (String path: paths){
            if (newPath.isEmpty()) {
                newPath = newPath + path;
                continue;
            }

            newPath = newPath + File.separator + path;
        }
        return newPath;
    }

    public static void removeAllNodes(List<Node> checkBoxList) {
        checkBoxList.removeAll(checkBoxList);
    }
}
