package org.green.kav.deploy;

import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;
import org.green.kav.deploy.config.JarDeployConfig;
import org.green.kav.deploy.controller.tab.DeleteFileTabPanelUIController;
import org.green.kav.deploy.controller.tab.DeployJarTabPanelUIController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class StageInitializer implements ApplicationListener<JarDeployUIApplication.StageReadyEvent> {
    private String applicationTitle;
    private ApplicationContext applicationContext;

    @Autowired
    private DeployJarTabPanelUIController deployJarTabPanelUIController;

    @Autowired
    private DeleteFileTabPanelUIController deleteFileTabPanelUIController;

    @Autowired
    private JarDeployConfig jarDeployConfig;

    @Autowired
    private TabPane mainTabPane;

    public StageInitializer(@Value("${spring.application.ui.title}") String applicationTitle, ApplicationContext applicationContext) {
        this.applicationTitle = applicationTitle;
        this.applicationContext = applicationContext;
    }

    @Override
    public void onApplicationEvent(JarDeployUIApplication.StageReadyEvent stageReadyEvent) {
        Stage stage = stageReadyEvent.getStage();
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(mainTabPane);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        stage.setScene(new Scene(scrollPane, 800, 600));
        stage.setTitle(applicationTitle);
        stage.show();
        deployJarTabPanelUIController.build(jarDeployConfig.getInitialDir());
        deleteFileTabPanelUIController.build(jarDeployConfig.getInitialDir());
    }
}
