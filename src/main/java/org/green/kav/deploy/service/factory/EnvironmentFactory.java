package org.green.kav.deploy.service.factory;

import org.green.kav.deploy.service.JarDeployer;
import org.green.kav.deploy.service.LocalhostJarDeployer;
import org.green.kav.deploy.service.RemoteHostJarDeployer;
import org.green.kav.deploy.service.model.LoggingManager;
import org.green.kav.fauna.client.model.Deployment;
import org.green.kav.move.remote.io.FileMover;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class EnvironmentFactory {
    private final Logger log = LoggerFactory.getLogger(EnvironmentFactory.class);

    private Deployment deployment;

    enum EnvironmentType {
        LOCAL, REMOTE
    }

    @Autowired
    private FileMover fileMover;

    @Autowired
    private LoggingManager loggingManager;

    public void setSelection(Deployment deployment) {
        this.deployment = deployment;
    }

    public JarDeployer getDeployer() {
        List<String> directories = new ArrayList<>();

        // Not defined!
        if (deployment == null) {
            throw new RuntimeException("Select a deployment site!");
        } else {
            log.info("Selected {}", deployment);
            // REMOTE
            if (deployment.getType().equalsIgnoreCase(EnvironmentType.REMOTE.name())) {
                return new RemoteHostJarDeployer(null, fileMover, deployment.getStagingDir(), loggingManager);
            } // LOCAL
            else if (deployment.getType().equalsIgnoreCase(EnvironmentType.LOCAL.name())) {
                directories.add(deployment.getDirectory());
            } // anything else!
            else {
                throw new RuntimeException(deployment.getType() + " is an UNKNOWN environment!");
            }
        }

        return new LocalhostJarDeployer(directories, loggingManager);
    }
}
