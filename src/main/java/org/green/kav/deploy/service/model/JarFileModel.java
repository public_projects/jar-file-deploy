package org.green.kav.deploy.service.model;

import java.io.File;
import java.util.Arrays;
import java.util.UUID;

public class JarFileModel {
    private final UUID id;
    private File deploymentFile;
    private String[] newDirectoryPaths;
    private String[] newRemoteTempDirectoryPath;
    private String type;
    private String version;
    private String symbolicName;
    private boolean selected = true;

    public JarFileModel(){
        this.id = UUID.randomUUID();
    }

    public UUID getId() {
        return id;
    }

    public void setSelected(boolean selected){
        this.selected = selected;
    }

    public boolean isSelected() {
        return this.selected;
    }

    public File getDeploymentFile() {
        return deploymentFile;
    }

    public void setDeploymentFile(File deploymentFile) {
        this.deploymentFile = deploymentFile;
    }

    public String[] getNewDirectoryPaths() {
        return newDirectoryPaths;
    }

    public void setNewDirectoryPaths(String[] newDirectoryPaths) {
        this.newDirectoryPaths = newDirectoryPaths;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSymbolicName() {
        return symbolicName;
    }

    public void setSymbolicName(String symbolicName) {
        this.symbolicName = symbolicName;
    }

    public String[] getNewRemoteTempDirectoryPath() {
        return newRemoteTempDirectoryPath;
    }

    public void setNewRemoteTempDirectoryPath(String[] newRemoteTempDirectoryPath) {
        this.newRemoteTempDirectoryPath = newRemoteTempDirectoryPath;
    }

    @Override
    public String toString() {
        return "JarFileModel{" +
               System.lineSeparator() + " deploymentFile = '" + deploymentFile + "'," +
               System.lineSeparator() + " newDirectoryPaths = '" + Arrays.toString(newDirectoryPaths) + "'," +
               System.lineSeparator() + " newRemoteTempDirectoryPath = '" + newRemoteTempDirectoryPath + "'," +
               System.lineSeparator() + " type= '" + type + "'," +
               System.lineSeparator() + " version = '" + version + "'," +
               System.lineSeparator() + " symbolicName ='" + symbolicName + "'," +
               System.lineSeparator() + "}";
    }
}
