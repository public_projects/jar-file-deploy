package org.green.kav.deploy.service.model;

import org.springframework.stereotype.Service;

@Service
public class LoggingManager {
    private StringBuilder error;
    private StringBuilder success;
    private StringBuilder commands;

    public LoggingManager() {
        this.error = new StringBuilder("*** ERROR ***");
        this.success= new StringBuilder("*** SUCCESS ***");
        this.commands = new StringBuilder("*** COMMAND ***");
    }

    public void reset() {
        this.error = new StringBuilder("*** ERROR ***");
        this.success = new StringBuilder("*** SUCCESS ***");
        this.commands = new StringBuilder("*** COMMAND ***");
    }

    public LoggingManager appendSuccess(String data){
        this.success.append(data);
        return this;
    }
    public LoggingManager appendError(String data){
        this.error.append(data);
        return this;
    }

    public LoggingManager appendCommand(String data){
        this.commands.append(data);
        return this;
    }

    public String print() {
        return success.append(System.lineSeparator()).append(error).append(System.lineSeparator()).append(commands).toString();
    }

    public String printError() {
        return error.toString();
    }

    public String printSuccess() {
        return error.toString();
    }
}
