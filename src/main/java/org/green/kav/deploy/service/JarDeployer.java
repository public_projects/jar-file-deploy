package org.green.kav.deploy.service;

import org.green.kav.deploy.service.model.JarFileModel;
import java.time.format.DateTimeFormatter;
import java.util.List;

public interface JarDeployer {
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    String deploy(List<JarFileModel> jarFileModel);
}
