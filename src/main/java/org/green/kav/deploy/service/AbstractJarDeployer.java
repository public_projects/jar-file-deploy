package org.green.kav.deploy.service;

import org.green.kav.deploy.service.model.JarFileModel;
import org.green.kav.deploy.service.model.LoggingManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

public abstract class AbstractJarDeployer implements JarDeployer {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected List<String> rootDirectory;
    protected LoggingManager loggingManager;

    public AbstractJarDeployer(List<String> directories, LoggingManager loggingManager) {
        this.rootDirectory = directories;
        this.loggingManager = loggingManager;
    }

    public String deploy(List<JarFileModel> jarFileModels) {
        for (String directory : rootDirectory) {
            checkDirectory(directory);

            for (JarFileModel jarFileModel : jarFileModels) {
                String newPath = "";
                for (String path: jarFileModel.getNewDirectoryPaths()){
                    newPath = newPath + File.separator + path;
                }

                File existingFile = new File(directory.trim() + File.separator + newPath);

                logger.info("isFile: {}.", existingFile.getAbsolutePath());
                logger.info("directory: {}.", directory.trim());
                logger.info("isNewDirectory: {}.", jarFileModel.getNewDirectoryPaths());

                if (existingFile.isFile()) {
                    deployFile(jarFileModel, existingFile);
                }
                else {
                    loggingManager.appendError(System.lineSeparator())
                              .appendError("original file: ")
                              .appendError(jarFileModel.getDeploymentFile()
                                                  .getAbsolutePath())
                              .appendError(System.lineSeparator())
                              .appendError("new Location: ")
                              .appendError(existingFile.getAbsolutePath())
                              .appendError(System.lineSeparator());
                }
            }
        }

        logger.error("{}", loggingManager.printError());
        logger.info("{}", loggingManager.printSuccess());

        LocalDateTime now = LocalDateTime.now(ZoneOffset.systemDefault());
        String formattedDate = now.format(dateTimeFormatter);

        String finalOutput = formattedDate + System.lineSeparator() + loggingManager.print();
        loggingManager.reset();
        return finalOutput;
    }

    public abstract void deployFile(JarFileModel jarFileModel, File existingFile);

    public static void checkDirectory(String directory) {
        File file = new File(directory);

        if (!file.isDirectory()) {
            throw new RuntimeException("Directory not found!" + file);
        }
    }
}
