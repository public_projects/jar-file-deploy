package org.green.kav.deploy.service.model;

public class RemoteDirectoryModel {
    private String environment;
    private String directory;

    public RemoteDirectoryModel(){
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }
}
