package org.green.kav.deploy.service;

import org.green.kav.deploy.service.model.JarFileModel;
import org.green.kav.deploy.service.model.LoggingManager;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

public class LocalhostJarDeployer extends AbstractJarDeployer {

    public LocalhostJarDeployer(List<String> directories, LoggingManager loggingManager) {
        super(directories, loggingManager);
    }

    @Override
    public void deployFile(JarFileModel jarFileModel, File existingFile) {
        Path from = jarFileModel.getDeploymentFile().toPath(); //convert from File to Path
        Path to = Paths.get(existingFile.getAbsolutePath()); //convert from String to Path
        try {
            Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
            loggingManager.appendError(System.lineSeparator())
                      .appendError("EX: original file: ")
                      .appendError(from.toAbsolutePath().toString())
                      .appendError(System.lineSeparator())
                      .appendError("EX: new Location: ")
                      .appendError(to.toAbsolutePath().toString())
                      .appendError(System.lineSeparator());
        }
        loggingManager.appendSuccess(System.lineSeparator())
                         .appendSuccess("from: ")
                         .appendSuccess(from.toAbsolutePath().toString())
                         .appendSuccess(System.lineSeparator())
                         .appendSuccess("to: ")
                         .appendSuccess(to.toAbsolutePath().toString())
                         .appendSuccess(System.lineSeparator());
    }
}
