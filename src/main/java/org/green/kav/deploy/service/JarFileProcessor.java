package org.green.kav.deploy.service;

import org.green.kav.deploy.service.model.JarFileModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class JarFileProcessor {
    private static final Logger logger = LoggerFactory.getLogger(JarFileProcessor.class);
    private static List<String> reserved;

    static {
        reserved = new ArrayList<>();
        reserved.add("Manifest-Version:");
        reserved.add("Bnd-LastModified:");
        reserved.add("Build-Jdk:");
        reserved.add("Built-By:");
        reserved.add("Bundle-Blueprint:");
        reserved.add("Bundle-Description:");
        reserved.add("Bundle-DocURL:");
        reserved.add("Bundle-ManifestVersion:");
        reserved.add("Bundle-Name:");
        reserved.add("Bundle-SymbolicName:");
        reserved.add("Bundle-Vendor:");
        reserved.add("Bundle-Version:");
        reserved.add("Created-By:");
        reserved.add("Export-Package:");
        reserved.add("Implementation-Version:");
        reserved.add("Import-Package:");
        reserved.add("Include-Resource:");
        reserved.add("Private-Package:");
        reserved.add("Require-Capability:");
        reserved.add("Tool:");
    }

    public Map<String, String> getConfig(List<String> fileConfig) {
        for (int i = 0; i < fileConfig.size(); i++) {
            String data = fileConfig.get(i);
            if (!contains(data, reserved)) {
                appendToPreviousLine(fileConfig, i, data);
            }
        }

        Map<String, String> keys = new HashMap<>();
        for (int i = 0; i < fileConfig.size(); i++) {
            String bundleConfig = fileConfig.get(i).trim();
            if (!bundleConfig.isEmpty()) {
                String configKey = bundleConfig.substring(0, bundleConfig.indexOf(":"));
                String configValue = bundleConfig.substring(bundleConfig.indexOf(":") + 1).trim();
                keys.put(configKey, configValue);
            }
        }

        return keys;
    }

    private boolean contains(String data, List<String> reservedKeys) {
        for (String reservedKey : reservedKeys) {
            if (data.contains(reservedKey)) {
                return true;
            }
        }
        return false;
    }

    private void appendToPreviousLine(List<String> dataLs, int currentIndex, String currentData) {
        int idx = currentIndex - 1;
        while (idx != -1) {
            String previousLineData = dataLs.get(idx);
            if (previousLineData.isEmpty()) {
                idx--;
                continue;
            }
            String newData = previousLineData + currentData.trim();
            dataLs.set(currentIndex, "");
            dataLs.set(idx, newData);
            break;
        }
    }

    public List<JarFileModel> findJar(File fileDirectories) {
        List<JarFileModel> jarFileModels = new ArrayList<>();
        try (Stream<Path> stream = Files.find(Paths.get(fileDirectories.getAbsolutePath()),
                                              5,
                                              (path, attr) -> path.getFileName().toString().contains(".jar"))) {

            stream.filter(path -> !path.toFile().getName().contains("-sources.jar")).forEach(path -> {
                System.err.println();
                String targetPath = path.getParent().toAbsolutePath().toString();
                System.out.println("jar path: " + targetPath);
                String parentPath = targetPath + File.separator + "classes";

                File classesDirectory = new File(parentPath);

                JarFileModel jarFileModel = new JarFileModel();

                if (classesDirectory.isDirectory()) {
                    String metaInfPath = parentPath + File.separator + "META-INF";
                    File metaInfDirectory = new File(metaInfPath);

                    if (metaInfDirectory.isDirectory()) {
                        logger.debug("Jar file: {}", metaInfDirectory.getAbsolutePath());
                        File manifestFile = new File(metaInfDirectory + File.separator + "MANIFEST.MF");
                        Map<String, String> config = new HashMap<>();
                        try {
                            config = getConfig(Files.readAllLines(manifestFile.toPath(), Charset.forName("utf-8")));
                        } catch (IOException e) {
                            logger.error("An error occurred");

                            if (e instanceof NoSuchFileException) {
                                logger.error("NoSuchFile: {}", ((NoSuchFileException) e).getFile());
                            }
                            else {
                                e.printStackTrace();
                            }
                        }

                        jarFileModel.setDeploymentFile(path.toFile());
                        if (manifestFile.isFile()) {
                            logger.debug("is a deployment jar!");
                            jarFileModel.setType("JAR");
                            jarFileModel.setSymbolicName(config.get("Bundle-SymbolicName"));
                            System.out.println("Bundle-SymbolicName: " + jarFileModel.getSymbolicName());
                            String implementationVersionValue = config.get("Implementation-Version");
                            implementationVersionValue = implementationVersionValue.substring(0, implementationVersionValue.indexOf("Build"));
                            jarFileModel.setVersion(implementationVersionValue.trim());
                        }
                        else {
                            String featureFilePath = parentPath + File.separator + "features.xml";
                            File featureFile = new File(featureFilePath);

                            if (featureFile.isFile()) {
                                jarFileModel = prepareFeatureFileDetails(targetPath, parentPath);
                                logger.info("Its a feature file!");
                            }
                            else {
                                logger.error("Not a deployment jar!");
                            }
                        }
                    }
                    else {
                        jarFileModel = prepareFeatureFileDetails(targetPath, parentPath);
                    }
                }

                if (jarFileModel.getType() != null) {
                    jarFileModel.setNewDirectoryPaths(preparePostfixDeploymentPath(jarFileModel));
                    jarFileModels.add(jarFileModel);
                    logger.info("{}",jarFileModel);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jarFileModels;
    }

    private JarFileModel prepareFeatureFileDetails(String targetPath, String parentPath) {
        JarFileModel jarFileModel = new JarFileModel();
        Properties prop = new Properties();
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(new File(
                    targetPath + File.separator + "maven-archiver" + File.separator + "pom.properties"));
            prop.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        jarFileModel.setVersion(prop.get("version").toString());
        jarFileModel.setSymbolicName(prop.get("groupId").toString());
        jarFileModel.setDeploymentFile(new File(parentPath + File.separator + "features.xml"));
        jarFileModel.setType("FEATURE_FILE");
        return jarFileModel;
    }

    public String replaceAllPeriodWithFileSeparator(String text) {
        String[] subDirs = text.split("\\.");

        StringBuilder newPathSb = new StringBuilder();
        for (String subDir : subDirs) {
            newPathSb.append(subDir).append(File.separator);
        }

        return newPathSb.toString();
    }

    public String[] preparePostfixDeploymentPath(JarFileModel jarFileModel) {
        List<String> partialNewDirPaths = Arrays.stream(jarFileModel.getSymbolicName().split("\\.")).collect(Collectors.toList());
        if (jarFileModel.getType().equals("JAR")) {
            partialNewDirPaths.add(jarFileModel.getVersion());
            partialNewDirPaths.add(jarFileModel.getDeploymentFile().getName());
            return partialNewDirPaths.toArray(new String[partialNewDirPaths.size()]);
        }
        else if (jarFileModel.getType().equals("FEATURE_FILE")) {
            partialNewDirPaths.add("features");
            partialNewDirPaths.add(jarFileModel.getVersion());
            partialNewDirPaths.add("features-" + jarFileModel.getVersion() + "-features.xml");
            return partialNewDirPaths.toArray(new String[partialNewDirPaths.size()]);
        }

        return null;
    }
}
