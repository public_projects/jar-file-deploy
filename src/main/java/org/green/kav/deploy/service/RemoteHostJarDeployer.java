package org.green.kav.deploy.service;

import org.green.kav.deploy.service.model.JarFileModel;
import org.green.kav.deploy.service.model.LoggingManager;
import org.green.kav.move.remote.io.FileMover;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

public class RemoteHostJarDeployer extends AbstractJarDeployer {
    private final String tempStagingDir;
    private final FileMover fileMover;

    public RemoteHostJarDeployer(List<String> directories,
                                 FileMover fileMover,
                                 String tempStagingDir,
                                 LoggingManager loggingManager) {
        super(directories, loggingManager);
        this.tempStagingDir = tempStagingDir;
        this.fileMover = fileMover;
    }

    public String deploy(List<JarFileModel> jarFileModels) {
        File tempDir = new File(tempStagingDir);
        StringBuilder errors = new StringBuilder("Failed to copy:");
        boolean hasFailure = false;
        for (JarFileModel jarFileModel : jarFileModels) {
            // Temporary deployment directory
            String tempRemoteArtifactDir = jarFileModel.getSymbolicName().replace(".", "_");
            String[] deloymentDirs = jarFileModel.getNewDirectoryPaths();
            String newFileName = deloymentDirs[deloymentDirs.length-1];
            String newFile = tempDir + File.separator + tempRemoteArtifactDir + File.separator + newFileName;
            String remoteFile = tempDir + "/" + tempRemoteArtifactDir +"/" + newFileName;
            String[] newRemoteTempDirectoryPath = {tempRemoteArtifactDir, newFileName};
            jarFileModel.setNewRemoteTempDirectoryPath(newRemoteTempDirectoryPath);

            loggingManager.appendSuccess(System.lineSeparator())
                          .appendSuccess("original file: ")
                          .appendSuccess(jarFileModel.getDeploymentFile().getAbsolutePath())
                          .appendSuccess(System.lineSeparator())
                          .appendSuccess("temp remote directory: ")
                          .appendSuccess(newFile)
                          .appendSuccess(System.lineSeparator());

            try {
                File tempFile = new File(newFile);
                tempFile.getParentFile().mkdirs();
                Files.copy(jarFileModel.getDeploymentFile().toPath(), tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ioException) {
                hasFailure = true;
                errors.append(System.lineSeparator()).append(newFile);
                ioException.printStackTrace();
            }

            if (hasFailure) {
                loggingManager.appendError(System.lineSeparator()).appendError(errors.toString());
            }
        }

        logger.error("{}", loggingManager.printError());
        logger.info("{}", loggingManager.printSuccess());

        LocalDateTime now = LocalDateTime.now(ZoneOffset.systemDefault());
        String formattedDate = now.format(dateTimeFormatter);
        String finalLog = formattedDate + System.lineSeparator() + loggingManager.print();
        loggingManager.reset();
        return finalLog;
    }

    @Override
    public void deployFile(JarFileModel jarFileModel, File existingFile) {

    }
}
