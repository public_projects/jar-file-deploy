package org.green.kav.deploy.config;

import javafx.scene.control.TabPane;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
public class JarDeployConfig {

    @Value("${jar.deploy.project.selection.initial.dir}")
    private String initialDir;

    @Value("${jar.deploy.single_mc.localhost.dir}")
    private String singleMcDir;

    @Value("${jar.deploy.multi_mc.localhost.dir}")
    private String multiMcDir;

    @Value("${jar.deploy.local_staging_dir}")
    private String localStagingDir;

    @Bean
    public TabPane getMainTabPane() {
        return new TabPane();
    }

    public String getInitialDir(){
        return initialDir;
    }

    public String getSingleMcDir() {
        return singleMcDir;
    }

    public String getMultiMcDir() {
        return multiMcDir;
    }

    public String getLocalStagingDir() {
        return localStagingDir;
    }
}
