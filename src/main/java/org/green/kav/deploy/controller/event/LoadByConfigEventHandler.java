package org.green.kav.deploy.controller.event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.FileChooser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LoadByConfigEventHandler implements EventHandler<ActionEvent> {
    private static final Logger logger = LoggerFactory.getLogger(LoadByConfigEventHandler.class);

    private final File initialDirectory;

    private final DeployDirectoryChooserEventHandler deployDirectoryChooserEventHandler;

    public LoadByConfigEventHandler(String initialDirectory, DeployDirectoryChooserEventHandler deployDirectoryChooserEventHandler){
        this.initialDirectory = new File(initialDirectory);
        this.deployDirectoryChooserEventHandler = deployDirectoryChooserEventHandler;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        logger.debug("Event been called!");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Default config", "*.config")
        );
        fileChooser.setInitialDirectory(initialDirectory);
        File selectedFile = fileChooser.showOpenDialog(null);

        if (selectedFile.isFile()) {
            logger.info("selected config: {}", selectedFile.getAbsolutePath());
            createNewProjectEvent(selectedFile);
        }
    }

    public void createNewProjectEvent(File file) {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            deployDirectoryChooserEventHandler.createNewProjectEvent(line);
            while (line != null) {
                System.out.println(line);
                // read next line
                line = reader.readLine();
                deployDirectoryChooserEventHandler.createNewProjectEvent(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
