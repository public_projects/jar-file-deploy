package org.green.kav.deploy.controller.event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;
import org.green.kav.deploy.service.JarFileProcessor;
import org.green.kav.deploy.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.green.kav.deploy.service.model.JarFileModel;

public class RefreshProjectEventHandler implements EventHandler<ActionEvent> {
    private static final Logger logger = LoggerFactory.getLogger(RefreshProjectEventHandler.class);
    private JarFileProcessor jarFileProcessor;
    private String absolutePath;
    private VBox jarDirectoriesVBox;
    private List<CheckBox> checkBoxList;
    private Map<UUID, JarFileModel> jarFileModelMap;
    private Button unselectAllButton;

    public RefreshProjectEventHandler(JarFileProcessor jarFileProcessor,
                                      String absolutePath,
                                      VBox jarDirectoriesVBox,
                                      List<CheckBox> checkBoxList,
                                      Map<UUID, JarFileModel> jarFileModelMap,
                                      Button unselectAllButton) {
        this.jarFileProcessor = jarFileProcessor;
        this.absolutePath = absolutePath;
        this.jarDirectoriesVBox = jarDirectoriesVBox;
        this.checkBoxList = checkBoxList;
        this.jarFileModelMap = jarFileModelMap;
        this.unselectAllButton = unselectAllButton;
    }


    @Override
    public void handle(ActionEvent actionEvent) {
        List<Node> nodes = jarDirectoriesVBox.getChildren();

        List<Node> checkBoxToRemoves = new ArrayList<>();

        for (Node node: nodes) {
            if (node instanceof CheckBox) {
                jarFileModelMap.remove(UUID.fromString(node.getId()));
                checkBoxToRemoves.add(node);
                logger.info("checkbox {}", node);
            }
        }
        jarDirectoriesVBox.getChildren().removeAll(checkBoxToRemoves);
        checkBoxList.removeAll(checkBoxList);
        Utils.createJarCheckBoxes(jarFileProcessor, jarFileModelMap, absolutePath, jarDirectoriesVBox, checkBoxList);

        if (checkBoxList.isEmpty()) {
            unselectAllButton.setVisible(false);
        }
    }
}
