package org.green.kav.deploy.controller.event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;

public class DeleteDirectoryChooserEventHandler implements EventHandler<ActionEvent> {
    private static final Logger logger = LoggerFactory.getLogger(DeleteDirectoryChooserEventHandler.class);

    private VBox deleteVerticalBox;
    private String initialDirectory;

    public DeleteDirectoryChooserEventHandler(VBox deleteVerticalBox, String initialDirectory) {
        this.deleteVerticalBox = deleteVerticalBox;
        this.initialDirectory = initialDirectory;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File(this.initialDirectory));
        directoryChooser.setTitle("Open Resource File");
        File selectedFile = directoryChooser.showDialog(null);

        String cssLayoutButton = "-fx-start-margin: 5;\n" +
                                 "-fx-end-margin: 5;\n" +
                                 "-fx-padding: 5";

        Button removeButton = new Button();
        removeButton.setText("Remove");
        removeButton.setStyle(cssLayoutButton);

        HBox newDeleteDirectoryBox = new HBox();
        String cssLayout = "-fx-border-color: gray;\n" +
                           "-fx-border-insets: 3;\n" +
                           "-fx-border-width: 1;\n" +
                           "-fx-border-style: solid;\n";


        newDeleteDirectoryBox.setPadding(new Insets(5, 5, 5, 5));
        newDeleteDirectoryBox.setSpacing(5);
        newDeleteDirectoryBox.setStyle(cssLayout);
        newDeleteDirectoryBox.getChildren().add(removeButton);
        newDeleteDirectoryBox.getChildren().add(new Label(selectedFile.getAbsolutePath()));
        deleteVerticalBox.getChildren().add(newDeleteDirectoryBox);

    }
}
