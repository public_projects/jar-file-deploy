package org.green.kav.deploy.controller.event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class OpenOnCmdEventHandler implements EventHandler<ActionEvent> {
    private String absolutePath;

    public OpenOnCmdEventHandler(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        try {
            // Just one line and you are done !
            // We have given a command to start cmd
            // /K : Carries out command specified by string
            Runtime.getRuntime().exec("cmd.exe /c start cd " + absolutePath);

        } catch (Exception e) {
            System.out.println("HEY Buddy ! U r Doing Something Wrong ");
            e.printStackTrace();
        }
    }
}
