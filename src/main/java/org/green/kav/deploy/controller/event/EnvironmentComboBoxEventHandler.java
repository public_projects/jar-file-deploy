package org.green.kav.deploy.controller.event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import org.green.kav.deploy.service.factory.EnvironmentFactory;
import org.green.kav.fauna.client.model.Deployment;
import org.green.kav.fauna.client.model.DeploymentResultSet;
import org.green.kav.fauna.client.service.DeploymentCacheService;

public class EnvironmentComboBoxEventHandler implements EventHandler<ActionEvent> {

    private ComboBox<String> comboBox;
    private Label label;
    private DeploymentCacheService service;
    private EnvironmentFactory environmentFactory;

    public EnvironmentComboBoxEventHandler(ComboBox<String> comboBox,
                                           Label label,
                                           DeploymentCacheService deploymentCacheService,
                                           EnvironmentFactory environmentFactory) {
        this.label = label;
        this.service = deploymentCacheService;
        this.comboBox = comboBox;
        this.environmentFactory = environmentFactory;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        DeploymentResultSet result = service.getByEnvironment(comboBox.getValue());

        if (result == null) throw new RuntimeException("No value found for given key: "+ comboBox.getValue());

        Deployment selectedDeployment = result.getDeployment();
        this.environmentFactory.setSelection(selectedDeployment);
        label.setText(selectedDeployment.getDirectory());
        label.setId(result.getId());
    }
}
