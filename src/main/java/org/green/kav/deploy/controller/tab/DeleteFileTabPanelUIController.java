package org.green.kav.deploy.controller.tab;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.VBox;
import org.green.kav.deploy.controller.event.DeleteDirectoryChooserEventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeleteFileTabPanelUIController {
    @Autowired
    private TabPane mainTabPane;

    public void build(String initialDirectory) {
        ScrollPane mainScrollPane = new ScrollPane();
        mainScrollPane.fitToWidthProperty().set(true);
        mainScrollPane.fitToHeightProperty().set(true);

        VBox deleteVerticalBox = new VBox();
        deleteVerticalBox.setSpacing(5);
        deleteVerticalBox.setPadding(new Insets(5, 5, 5, 5));

        mainScrollPane.setContent(deleteVerticalBox);

        Tab deleteFileTab = new Tab();
        deleteFileTab.setContent(mainScrollPane);
        deleteFileTab.setText("Delete files");
        deleteFileTab.setClosable(false);

        mainTabPane.getTabs().add(deleteFileTab);

        DeleteDirectoryChooserEventHandler deleteDirectoryChooserEventHandler = new DeleteDirectoryChooserEventHandler(deleteVerticalBox,
                                                                                                                       initialDirectory);

        Button chooseProjectsButton = new Button("Choose projects");
        chooseProjectsButton.setOnAction(deleteDirectoryChooserEventHandler);
        deleteVerticalBox.getChildren().add(chooseProjectsButton);
    }
}
