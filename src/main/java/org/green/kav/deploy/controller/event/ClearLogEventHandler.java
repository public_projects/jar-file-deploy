package org.green.kav.deploy.controller.event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextArea;
import org.green.kav.deploy.service.model.LoggingManager;

public class ClearLogEventHandler implements EventHandler<ActionEvent> {
    private LoggingManager loggingManager;
    private TextArea logTextArea;

    public ClearLogEventHandler(TextArea logTextArea, LoggingManager loggingManager) {
        this.loggingManager = loggingManager;
        this.logTextArea = logTextArea;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        loggingManager.reset();
        this.logTextArea.setText("");
    }
}
