package org.green.kav.deploy.controller.event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import org.green.kav.deploy.service.JarFileProcessor;
import org.green.kav.deploy.service.model.JarFileModel;
import org.green.kav.deploy.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.util.*;
import java.util.List;

public class DeployDirectoryChooserEventHandler implements EventHandler<ActionEvent> {
    private static final Logger logger = LoggerFactory.getLogger(DeployDirectoryChooserEventHandler.class);

    private File initialDirectory;
    private VBox vBox;
    private JarFileProcessor jarFileProcessor;
    private Map<UUID, JarFileModel> jarFileModelMap;

    public DeployDirectoryChooserEventHandler(VBox vBox, String initialDirectory, JarFileProcessor jarFileProcessor) {
        this.vBox = vBox;
        this.initialDirectory = new File(initialDirectory);
        this.jarFileProcessor = jarFileProcessor;
        this.jarFileModelMap = new HashMap<>();
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        logger.debug("Event been called!");

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Open Resource File");
        directoryChooser.setInitialDirectory(initialDirectory);
        File selectedFile = directoryChooser.showDialog(null);

        if (selectedFile.isDirectory()) {
            createNewProjectEvent(selectedFile.getAbsolutePath());
        }
    }

    public void createNewProjectEvent(String absolutePath) {
        HBox selectedJarHeaderPane = new HBox();
        selectedJarHeaderPane.setId("RemoveProjectBar");
        selectedJarHeaderPane.setPadding(new Insets(5, 5, 5, 20));
        selectedJarHeaderPane.setSpacing(5);

        String cssLayout = "-fx-border-color: gray;\n" +
                           "-fx-border-insets: 5;\n" +
                           "-fx-border-width: 1;\n" +
                           "-fx-border-style: solid;\n";

        selectedJarHeaderPane.setStyle(cssLayout);

        Button refreshProjectButton = new Button("Reload");
        Button unselectAllButton = new Button("Unselect All");
        VBox jarDirectoriesVBox = new VBox();
        jarDirectoriesVBox.setPadding(new Insets(4, 4, 4, 50));
        jarDirectoriesVBox.setSpacing(5);

        HBox jarDirectoriesButtonSelectionsHBox = new HBox();
        jarDirectoriesButtonSelectionsHBox.setSpacing(5);
        jarDirectoriesButtonSelectionsHBox.getChildren().add(unselectAllButton);

        jarDirectoriesVBox.getChildren().add(jarDirectoriesButtonSelectionsHBox);

        List<CheckBox> checkBoxList = new ArrayList<>();

        refreshProjectButton.setOnAction(new RefreshProjectEventHandler(this.jarFileProcessor,
                                                                        absolutePath,
                                                                        jarDirectoriesVBox,
                                                                        checkBoxList,
                                                                        this.jarFileModelMap,
                                                                        unselectAllButton));

        Utils.createJarCheckBoxes(jarFileProcessor, jarFileModelMap, absolutePath, jarDirectoriesVBox, checkBoxList);

        if (checkBoxList.isEmpty()) {
            unselectAllButton.setVisible(false);
        }

        unselectAllButton.setOnAction(actionEvent -> {
            if (!checkBoxList.isEmpty()) {
                boolean selectionMode = false;

                if (unselectAllButton.getText().equalsIgnoreCase("Unselect All")) {
                    selectionMode = false;
                    unselectAllButton.setText("Select All");
                }
                else {
                    selectionMode = true;
                    unselectAllButton.setText("Unselect All");
                }

                for (CheckBox checkBox : checkBoxList) {
                    checkBox.setSelected(selectionMode);
                    JarFileModel temp = jarFileModelMap.get(UUID.fromString(checkBox.getId()));
                    temp.setSelected(selectionMode);
                    jarFileModelMap.put(temp.getId(), temp);
                }
            }
        });

        VBox jarSelectionPaneGoupVBox = new VBox();
        Button removeProjectButton = new Button("Remove Project");
        Button openCmd = new Button("CMD");
        openCmd.setOnAction(new OpenOnCmdEventHandler(absolutePath));
        removeProjectButton.setOnAction(new RemoveProjectEventHandler(vBox, jarSelectionPaneGoupVBox, jarFileModelMap));
        Label projectPathLabel = new Label(absolutePath);

        selectedJarHeaderPane.getChildren().add(refreshProjectButton);
        selectedJarHeaderPane.getChildren().add(openCmd);
        selectedJarHeaderPane.getChildren().add(removeProjectButton);
        selectedJarHeaderPane.getChildren().add(projectPathLabel);

        jarSelectionPaneGoupVBox.getChildren().add(selectedJarHeaderPane);
        jarSelectionPaneGoupVBox.getChildren().add(jarDirectoriesVBox);

        vBox.getChildren().add(jarSelectionPaneGoupVBox);
    }

    public Map<UUID, JarFileModel> getSelectedModels(){
        return this.jarFileModelMap;
    }

}
