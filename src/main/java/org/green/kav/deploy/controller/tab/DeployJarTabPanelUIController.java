package org.green.kav.deploy.controller.tab;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.green.kav.deploy.controller.event.ClearLogEventHandler;
import org.green.kav.deploy.controller.event.DeployDirectoryChooserEventHandler;
import org.green.kav.deploy.controller.event.DeployJarEventHandler;
import org.green.kav.deploy.controller.event.EnvironmentComboBoxEventHandler;
import org.green.kav.deploy.controller.event.LoadByConfigEventHandler;
import org.green.kav.deploy.service.JarFileProcessor;
import org.green.kav.deploy.service.factory.EnvironmentFactory;
import org.green.kav.deploy.service.model.LoggingManager;
import org.green.kav.fauna.client.model.DeploymentResultSet;
import org.green.kav.fauna.client.service.DeploymentCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.concurrent.ExecutionException;

@Component
public class DeployJarTabPanelUIController {

    @Autowired
    private TabPane mainTabPane;

    @Autowired
    private JarFileProcessor jarFileProcessor;

    @Autowired
    private LoggingManager loggingManager;

    @Autowired
    private DeploymentCacheService deploymentCacheService;

    @Autowired
    private EnvironmentFactory environmentFactory;

    public void build(String initialDirectory) {
        VBox directorySelectionVbox = new VBox();
        DeployDirectoryChooserEventHandler
                deployDirectoryChooserEventHandler = new DeployDirectoryChooserEventHandler(directorySelectionVbox,
                                                                                            initialDirectory,
                                                                                            jarFileProcessor);

        ComboBox<String> comboBox = new ComboBox();
        generateComboBoxValues(comboBox);

        Label selected = new Label("default item selected");
        selected.setPadding(new Insets(5, 5, 5, 5));
        // Set on action
        comboBox.setOnAction(new EnvironmentComboBoxEventHandler(comboBox, selected, deploymentCacheService, environmentFactory));

        LoadByConfigEventHandler loadByConfigEventHandler = new LoadByConfigEventHandler(initialDirectory, deployDirectoryChooserEventHandler);

        Button loadByConfigButton = new Button("Load By Config");
        loadByConfigButton.setOnAction(loadByConfigEventHandler);

        Button chooseProjectsButton = new Button("Choose projects");
        chooseProjectsButton.setOnAction(deployDirectoryChooserEventHandler);

        VBox mainSelectionVbox = new VBox();
        mainSelectionVbox.setSpacing(5);
        mainSelectionVbox.setPadding(new Insets(5, 5, 5, 5));
        mainSelectionVbox.getChildren().add(comboBox);
        mainSelectionVbox.getChildren().add(selected);
        mainSelectionVbox.getChildren().add(loadByConfigButton);
        mainSelectionVbox.getChildren().add(chooseProjectsButton);

        VBox foundDirectoriesVBox = new VBox();
        foundDirectoriesVBox.setSpacing(5);
        foundDirectoriesVBox.setPadding(new Insets(5, 5, 5, 5));

        HBox selectedJarHeaderPane = new HBox();
        selectedJarHeaderPane.setId("Deploy Jar box");
        selectedJarHeaderPane.setPadding(new Insets(5, 0, 5, 0));
        selectedJarHeaderPane.setSpacing(5);

        TextArea logTextArea = new TextArea();
        logTextArea.setPrefHeight(400);
        logTextArea.setMinHeight(150);

        DeployJarEventHandler deployJarEventHandler = new DeployJarEventHandler(environmentFactory, deployDirectoryChooserEventHandler, logTextArea);

        Button deployJarsButton = new Button("Deploy Jars");
        deployJarsButton.setOnAction(deployJarEventHandler);

        Button deployTestJarButton = new Button("Deploy Test");
        deployTestJarButton.setOnAction(deployJarEventHandler);

        selectedJarHeaderPane.getChildren().add(deployJarsButton);
        selectedJarHeaderPane.getChildren().add(deployTestJarButton);

        String cssLayout = "-fx-border-color: red;\n" +
                           "-fx-border-insets: 5;\n" +
                           "-fx-border-width: 1;\n" +
                           "-fx-border-style: dashed;\n";
        VBox executionVbox = new VBox();
        executionVbox.setStyle(cssLayout);
        executionVbox.setFillWidth(true);
        executionVbox.setPadding(new Insets(5, 5, 5, 5));
        executionVbox.setSpacing(5);
        executionVbox.getChildren().add(selectedJarHeaderPane);
        executionVbox.getChildren().add(logTextArea);

        Button clearLog = new Button("Clear log");
        clearLog.setOnAction(new ClearLogEventHandler(logTextArea, loggingManager));
        executionVbox.getChildren().add(clearLog);

        VBox mainPane = new VBox();
        mainPane.getChildren().add(mainSelectionVbox);
        mainPane.getChildren().add(directorySelectionVbox);
        mainPane.getChildren().add(foundDirectoriesVBox);
        mainPane.getChildren().add(executionVbox);

        ScrollPane mainScrollPane = new ScrollPane();
        mainScrollPane.fitToWidthProperty().set(true);
        mainScrollPane.fitToHeightProperty().set(true);
        mainScrollPane.setContent(mainPane);

        Tab deployJarTab = new Tab();
        deployJarTab.setContent(mainScrollPane);
        deployJarTab.setText("Deploy Jar");
        deployJarTab.setClosable(false);

        mainTabPane.getTabs().add(deployJarTab);
    }

    private void generateComboBoxValues(ComboBox<String> comboBox) {
        try {
            for (DeploymentResultSet model : deploymentCacheService.getAll()) {
                comboBox.getItems().add(model.getDeployment().getEnvironment());
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            Platform.exit();
        }
    }
}
