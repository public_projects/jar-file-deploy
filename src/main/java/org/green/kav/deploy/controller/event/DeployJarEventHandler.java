package org.green.kav.deploy.controller.event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextArea;
import org.green.kav.deploy.service.JarDeployer;
import org.green.kav.deploy.service.factory.EnvironmentFactory;
import org.green.kav.deploy.service.model.JarFileModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.stream.Collectors;

public class DeployJarEventHandler implements EventHandler<ActionEvent> {
    private static final Logger logger = LoggerFactory.getLogger(DeployJarEventHandler.class);

    private DeployDirectoryChooserEventHandler deployDirectoryChooserEventHandler;

    private TextArea deploymentLog;

    private EnvironmentFactory environmentFactory;

    public DeployJarEventHandler(EnvironmentFactory environmentFactory,
                                 DeployDirectoryChooserEventHandler deployDirectoryChooserEventHandler,
                                 TextArea deploymentLog) {
        this.environmentFactory = environmentFactory;
        this.deployDirectoryChooserEventHandler = deployDirectoryChooserEventHandler;
        this.deploymentLog = deploymentLog;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        JarDeployer jarDeployer;
        try {
            jarDeployer = environmentFactory.getDeployer();
            String log = jarDeployer.deploy(deployDirectoryChooserEventHandler.getSelectedModels()
                                                                              .values()
                                                                              .stream()
                                                                              .filter(JarFileModel::isSelected)
                                                                              .collect(Collectors.toList()));
            deploymentLog.setText(log);
        } catch(Exception ex) {
            deploymentLog.setText(ex.getMessage());
        }
    }
}
