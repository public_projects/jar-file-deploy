package org.green.kav.deploy.controller.event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;
import org.green.kav.deploy.service.model.JarFileModel;
import java.util.Map;
import java.util.UUID;

public class RemoveProjectEventHandler implements EventHandler<ActionEvent> {
    private VBox jarSelectionPaneGoupVBox;
    private VBox vBox;
    private Map<UUID, JarFileModel> jarFileModelMap;

    public RemoveProjectEventHandler(VBox vBox, VBox jarSelectionPaneGoupVBox, Map<UUID, JarFileModel> jarFileModelMap) {
        this.vBox = vBox;
        this.jarSelectionPaneGoupVBox = jarSelectionPaneGoupVBox;
        this.jarFileModelMap = jarFileModelMap;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        this.jarSelectionPaneGoupVBox.getChildren().forEach(node -> {
            if (node instanceof VBox) {
                ((VBox) node).getChildren().forEach(node1 -> {
                    if (node1 instanceof CheckBox) {
                        jarFileModelMap.remove(UUID.fromString(node1.getId()));
                    }
                });
            }
        });
        this.vBox.getChildren().removeAll(jarSelectionPaneGoupVBox);
    }
}
