package org.green.kav.deploy.controller.event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BuildProjectEventHandler implements EventHandler<ActionEvent> {

    private String absolutePath;

    public BuildProjectEventHandler(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        try {
            ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "cd " + absolutePath + " && mvn clean install -DskipTests");
            builder.redirectErrorStream(true);
            Process p = null;
            p = builder.start();

            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while (true) {
                line = r.readLine();
                if (line == null) { break; }
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
