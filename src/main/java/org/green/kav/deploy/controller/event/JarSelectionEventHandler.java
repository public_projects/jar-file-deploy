package org.green.kav.deploy.controller.event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import org.green.kav.deploy.service.model.JarFileModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JarSelectionEventHandler implements EventHandler<ActionEvent> {
    private static final Logger logger = LoggerFactory.getLogger(JarSelectionEventHandler.class);
    private JarFileModel jarFileModel;

    public JarSelectionEventHandler(JarFileModel jarFileModel) {
        this.jarFileModel = jarFileModel;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        jarFileModel.setSelected(!jarFileModel.isSelected());
        if (jarFileModel.isSelected()) {
            logger.info("[{}, {}]", jarFileModel.getId(), jarFileModel.getNewDirectoryPaths(), jarFileModel.isSelected());
        }
    }
}
